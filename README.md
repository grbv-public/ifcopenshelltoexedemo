# IfcOpenShellToExeDemo

Dieses Repository dient zur Demonstration folgender Aspekte:
* Erstellen einer eigenständigen exe-Datei, die IfcOpenShell-Funktionalität unabhängig einer lokalen Python- oder BlenderBim-Installation ermöglicht, mittels des Pakets pyinstaller.
* Verschieben von Elementen in einer IFC-Datei mittels der IfcOpenShell-API-Funktionen aggregate.assign_object bzw. unassign_object (als Alternative zu den Blender-Funktionen link und unlink).

## Erforderliche Schritte

### Installation der benötigten Pakete
Z.B. mit pipenv (nutzt Pipfile und Pipfile.lock):

```
pipenv install
```

Oder mit pip:

```
pip install -r requirements.txt
```

### Erzeugen einer exe-Datei
```
pyinstaller main.spec
``` 

## Erläuterung
### Specfile
Die Datei main.spec beinhaltet die Konfiguration für pyinstaller. Die entscheidende Stelle für IfcOpenShell befindet sich in Zeile 4:

```python
datas = [(f'{sysconfig.get_path("purelib")}\\ifcopenshell','ifcopenshell')]
```

Hiermit wird das komplette IfcOpenShell-Paket in die exe-Datei kopiert. Der Grund dafür ist, dass es in IfcOpenShell Code gibt, der einzelne Python-Dateien zur Laufzeit nachlädt. Um die Dateigröße der exe-Datei zu verringern, wäre es wahrscheinlich auch denkbar, hier selektiver vorzugehen und nur die benötigten Dateien einzeln zu kopieren.

### Funktionsweise des Skripts
Das Skript, bzw. später dann die exe-Datei, iteriert über alle IFC-Dateien in dem Verzeichnis, in dem es ausgeführt wird. Für jedes IfcBuilding darin wird überprüft, ob es sich auf einer IfcSite befindet, dessen Name mit dem gleichen Zeichen beginnt. Wenn das nicht der Fall ist, wird eine passende IfcSite gesucht und das IfcBuilding ggf. dorthin verschoben.

Anmerkung: Da dieses Repository hauptsächlich zur Demonstration der Funktionsweise von pyinstaller dient, handelt es sich um ein funktional stark eingeschränktes Skript. Das Kopieren der passenden IfcSite aus einem Template wird hier nicht berücksichtigt.