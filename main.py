import glob
import os
import ifcopenshell
import ifcopenshell.util.selector


def move_building_to_site(ifc_file, building, site):
    # Find site containing building
    old_site = ifcopenshell.util.element.get_aggregate(building)

    # Remove building from its original site
    ifcopenshell.api.run("aggregate.unassign_object",
                         ifc_file, product=building, relating_object=old_site)

    # Add building to the intended new site
    ifcopenshell.api.run("aggregate.assign_object",
                         ifc_file, product=building, relating_object=site)


def find_site_for_building(ifc_file, building):
    filter_result: set = ifcopenshell.util.selector.filter_elements(ifc_file, f"IfcSite, Name=/{building.Name[0]}.*/")
    if len(filter_result) == 1:
        [site] = filter_result
        return site

    print(f'IfcSite für das IfcBuilding {building.Name} konnte nicht eindeutig identifiziert werden.')


ifc_files = glob.glob('*.ifc')

for ifc_file in ifc_files:
    print(f'Verarbeite {ifc_file}')
    ifc_model = ifcopenshell.open(ifc_file)

    for building in ifc_model.by_type('IfcBuilding'):
        site = ifcopenshell.util.element.get_aggregate(building)
        if building.Name[0] != site.Name[0]:
            print(f'{building.Name} befindet sich auf der falschen IfcSite')
            correct_site = find_site_for_building(ifc_file, building)
            if correct_site:
                move_building_to_site(ifc_file, building, correct_site)

    ifc_model.write(f'{os.path.splitext(ifc_file)[0]}_modifiziert.ifc')

